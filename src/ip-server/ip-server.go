package main

import (
	"flag"
	"llvc/universal"
	"log"
)

var pConfig universal.Config
var Clients universal.ThreadSaveMap

func main() {
	var err error
	universal.SetLogFile("../logs/ip_parser.log")
	Clients.Init()
	Comms.Init()
	configFile := flag.String("config-file", "../ressources/default/ip_parser/config.json", "filepath of the config file")
	flag.Parse()
	pConfig, err = universal.LoadConfig(*configFile)

	if err != nil {
		log.Fatalln(err)
	}

	listener := universal.NewListener(pConfig)
	go HandleInput()
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("server: accept: %s", err)
			continue
		}

		log.Printf("server: accepted from %s", conn.RemoteAddr())
		go HandleClient(conn)
	}

}
