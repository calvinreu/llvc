package main

import (
	"fmt"
	"llvc/universal"
	"log"
	"net"
	"time"
)

func EstablishClient(conn net.Conn, Client universal.Profile) []byte {

	if Clients.Len() >= pConfig.MaxUsers {
		conn.Write([]byte("err: the maximum count of clients has been reached" + fmt.Sprint(pConfig.MaxUsers)))
		log.Println("the maximum count of clients has been reached")
		return nil
	}

	data, err := Clients.Marshal()
	if err != nil {
		log.Fatalf("server: EstablisClient: %s", err)
	}

	conn.SetDeadline(time.Now().Add(10 * time.Second))
	_, err = conn.Write(data)
	if err != nil {
		log.Printf("server: EstablishClient: %s", err)
		return nil
	}
	Clients.AddUnsave(Client.IP, Client)

	return data
}

func HandleClient(conn net.Conn) {
	var Client universal.Profile
	defer conn.Close()
	buffer := make([]byte, BufferSize)
	conn.SetDeadline(time.Now().Add(time.Duration(pConfig.ConnectionTimeout) * time.Second))
	messageLen, err := conn.Read(buffer) //send profile
	if err != nil {
		log.Printf("server: HandleClient: %s", err)
		return
	}

	err = Client.Unmarshal(buffer[:messageLen])
	if err != nil {
		conn.SetDeadline(time.Now().Add(time.Duration(pConfig.ConnectionTimeout) * time.Second))
		conn.Write([]byte("err: unable to unmarshal user profile"))
		log.Printf("server: HandleClient: %s", err)
		return
	}
	Clients.Lock()
	data := EstablishClient(conn, Client)
	Clients.Unlock()
	if data == nil {
		return
	}

	Comms.Lock()
	Comms.SendUnsave("c" + Client.IP)
	Comms.AddUserUnsave(Client.IP)
	log.Printf("HandleClient: added profile %s", Client)
	Comms.Unlock()
	ConnectionLoop(conn, buffer, Client.IP)
	Clients.Remove(Client.IP)
	Comms.RemoveUser(Client.IP)
	Comms.Send("d" + Client.IP)
	log.Printf("Connection %s closed", conn.RemoteAddr().String())

}
