package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func GetInputString() string {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("[llvc-server]: ")
	text, _ := reader.ReadString('\n')
	text = strings.ReplaceAll(text, " ", "")
	return strings.ReplaceAll(text, "\n", "")
}

func HandleInput() {
	for {
		switch GetInputString() {
		case "quit":
			os.Exit(0)
		case "exit":
			os.Exit(0)
		case "config":
			fmt.Println(pConfig.NetworkProtocol + " at :" + pConfig.IP + " with max user count " + fmt.Sprint(pConfig.MaxUsers))
		default:
			fmt.Println("unknown command")
		}
	}
}
