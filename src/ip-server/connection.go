package main

import (
	"llvc/universal"
	"log"
	"net"
	"time"
)

const (
	BufferSize = 8192
)

var EmptyMessage = []byte("-")

var Comms universal.SharedChannel

func ConnectionLoop(conn net.Conn, buffer []byte, ID string) {
	for {
		conn.SetDeadline(time.Now().Add(time.Second * time.Duration(pConfig.PullTimer)))
		messageLen, err := conn.Read(buffer)
		if err != nil {
			log.Printf("server: ConnectionLoop: %s", err)
			return
		}
		if messageLen == 2 {
			Reply(conn, ID)
			continue
		}

		switch string(buffer[:1]) {
		case "d": //disconnect
			return
		case "r":
			Comms.GetChannel(string(buffer[1:messageLen])) <- string(buffer[:messageLen])
		}
		Reply(conn, ID)
	}
}

func Reply(conn net.Conn, ID string) {
	select {
	case client := <-Comms.GetChannel(ID):
		_, err := conn.Write([]byte(client))
		if err != nil {
			log.Printf("server: Reply: %s", err)
			return
		}
	case <-time.After(time.Second * time.Duration(pConfig.ConnectionTimeout/2)):
		conn.Write([]byte("-"))
	}
}
