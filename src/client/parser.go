package main

import (
	"log"
	"net"
	"time"
)

func HandleParser(conn net.Conn) {
	buffer := make([]byte, BufferSize)
	for {
		select {
		case c := <-Comms.GetChannel("0"):
			b := []byte(c)
			conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
			_, err := conn.Write(b)
			if err != nil {
				log.Printf("Handle: parser: %s", err)
				//handle error reconnect to parser
			}
		case <-time.After(time.Second * time.Duration(Config.PullTimer-Config.ConnectionTimeout)): //-ConnectionTimeout to leave time for the message to get to the parser
			conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
			_, err := conn.Write(make([]byte, 0))
			if err != nil {
				log.Printf("Handle: parser: %s", err)
				//handle error reconnect to parser
			}
		}
		conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
		messageLen, err := conn.Read(buffer)
		//Connection timeout
		if err != nil {
			log.Printf("Handle: parser: %s this will probably cause this programm to not work", err)
			//handle error reconnect to parser
		}
		if messageLen > 1 {
			Comms.GetChannel(string(buffer[1:messageLen])) <- string(buffer[:1])
		}
	}
}
