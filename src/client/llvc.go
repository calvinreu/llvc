package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"llvc/universal"
	"log"
	"time"
)

var Config universal.Config
var Profile universal.Profile
var Comms universal.SharedChannel
var AudioInput universal.ThreadSaveData
var AudioOutput universal.ThreadSaveMapD

const (
	BufferSize = 8192
)

func main() {
	var Users map[string]universal.Profile
	Logfile := flag.String("log_file", "../logs/client.log", "")
	configFile := flag.String("config_file", "../ressources/default/client/config.json", "filepath of the config file")
	profileFile := flag.String("profile_file", "../ressources/default/client/profile.json", "filepath of the profile file")
	flag.Parse()
	universal.SetLogFile(*Logfile)
	defer log.Println("Exited")
	err := Profile.Load(*profileFile)
	if err != nil {
		fmt.Println(*profileFile + " error: " + err.Error())
		log.Fatalf("client: main: loadprofile: %s", err)
		return
	}

	Comms.Init()
	AudioOutput.Init(BufferSize)

	Config, err = universal.LoadConfig(*configFile)
	if err != nil {
		fmt.Println(*configFile + " error: " + err.Error())
		log.Fatalf("client: main: loadconfig: %s", err)
		return
	}

	tmpConf := Config
	tmpConf.IP = "127.0.0.1:0"
	listener := universal.NewListener(tmpConf)
	Profile.IP = listener.Addr().String()
	buffer, err := Profile.Marshal()
	if err != nil {
		log.Fatalf("client: main: marshalprofile: %s", err)
	}
	ParserConn := universal.Dial(Config)

	ParserConn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
	_, err = ParserConn.Write(buffer)
	if err != nil {
		log.Fatalf("client: main: %s", err)
	}
	buffer = make([]byte, BufferSize)
	ParserConn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
	messageLen, err := ParserConn.Read(buffer)
	if err != nil {
		log.Fatalf("client: main: %s", err)
	}
	err = json.Unmarshal(buffer[:messageLen], &Users)
	if err != nil {
		log.Fatalf("client: main: %s", err)
	}

	Comms.AddUserUnsave("0") //parser
	for _, User := range Users {
		go RequestConn(User)
	}

	log.Printf("client: Connected to parser user list: %s", Users)
	go HandleParser(ParserConn)
	go ServerListen(listener)
	go RunMicrophone()
	HandleInput()

	ParserConn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
	Comms.Send("d")
	if err != nil {
		log.Fatalf("client: main: %s", err)
	}
	time.Sleep(1000 * time.Millisecond) //time to finish loging
}
