package main

import (
	"encoding/json"
	"llvc/universal"
	"log"
	"net"
	"time"
)

func VerifyConn(conn net.Conn) {
	buffer := make([]byte, BufferSize)
	defer conn.Close()

	conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
	len, err := conn.Read(buffer)
	if err != nil {
		log.Printf("VerifyConn: %s", err)
		return
	}
	var tmpUser universal.Profile
	err = json.Unmarshal(buffer[0:len], &tmpUser)
	if err != nil {
		log.Printf("VerifyConn: %s", err)
	}
	Comms.AddUser(tmpUser.IP)
	defer Comms.RemoveUser(tmpUser.IP)
	Comms.GetChannel("0") <- "--"

	select {
	case <-time.After(time.Second * time.Duration(Config.ConnectionTimeout)):
		return
	case <-Comms.GetChannel(tmpUser.IP):
		HandleConn(conn, tmpUser)
	}
}

func ServerListen(listener net.Listener) {
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("ServerListen: %s", err)
			continue
		}

		log.Printf("server: accepted from %s", conn.RemoteAddr())
		go VerifyConn(conn) //Handle included
	}
}
