package main

import (
	"encoding/json"
	"llvc/universal"
	"log"
	"net"
	"time"
)

func RequestConn(User universal.Profile) {
	conn := universal.DialIP(Config, User.IP)
	if conn == nil {
		log.Printf("HandleConn: %s", "connection returned is nil")
		Comms.GetChannel("0") <- "r" + User.IP
		return
	}
	defer conn.Close()
	Comms.AddUser(User.IP)
	defer Comms.RemoveUser(User.IP)
	data, err := json.Marshal(Profile)
	if err != nil {
		log.Printf("RequestConn: %s", err)
	}
	conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
	conn.Write(data)
	if err != nil {
		log.Printf("HandleConn: %s", err)
		Comms.GetChannel("0") <- "r" + User.IP
		return
	}
	HandleConn(conn, User)
}

func HandleConn(conn net.Conn, User universal.Profile) {
	log.Printf("HandleConn: User: %s is being handled", User.Name)
	defer log.Printf("HandleConn: User: %s is not being handled anymore", User.Name)
	for {
		timestamp := time.Now()
		select {
		case command := <-Comms.GetChannel(User.IP):
			switch command {
			case "d":
				return
			case "r":
				//wtf definetly some form of logic error I dont get how it should handle it
			}
		default:
		}
		conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
		_, err := conn.Read(AudioOutput.At(User.Name))
		if err != nil {
			log.Printf("HandleConn: %s", err)
			Comms.GetChannel("0") <- "r" + User.IP
			return
		}
		_, err = conn.Write(AudioInput.Read())
		conn.SetDeadline(time.Now().Add(time.Second * time.Duration(Config.ConnectionTimeout)))
		if err != nil {
			log.Printf("HandleConn: %s", err)
			Comms.GetChannel("0") <- "r" + User.IP
			return
		}
		if time.Since(timestamp) < 2*time.Millisecond {
			time.Sleep(2 * time.Millisecond)
		}
	}
}
