package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func GetInputString() string {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("[llvc-client]: ")
	text, _ := reader.ReadString('\n')
	text = strings.ReplaceAll(text, " ", "")
	return strings.ReplaceAll(text, "\n", "")
}

func HandleInput() {
	for {
		switch GetInputString() {
		case "quit":
			return
		case "exit":
			return
		case "parser-test":
			Comms.AddUser(Profile.IP) //standardize to compare to others
			fmt.Println(Comms.ListIndex())
		default:
			fmt.Println("unknown command")
		}
	}
}
