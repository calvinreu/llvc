package universal

import (
	"crypto/rand"
	"crypto/tls"
	"log"
	"net"
)

func DialIP(pConfig Config, IP string) net.Conn {
	pConfig.IP = IP
	log.Printf("dial: TLS Cert: %s", pConfig.TlsCrt)
	cert, err := tls.LoadX509KeyPair(pConfig.TlsCrt, pConfig.TlsKey)
	if err != nil {
		log.Printf("dial: loadkeys: %s", err)
		return nil
	}
	config := tls.Config{Certificates: []tls.Certificate{cert}, InsecureSkipVerify: true}
	conn, err := tls.Dial(pConfig.NetworkProtocol, pConfig.IP, &config)
	if err != nil {
		log.Printf("dial: %s", err)
		return nil
	}
	log.Println("dial: connected to: ", conn.RemoteAddr())

	state := conn.ConnectionState()
	log.Println("dial: handshake: ", state.HandshakeComplete)

	return conn
}

func Dial(pConfig Config) net.Conn {
	log.Printf("dial: TLS Cert: %s", pConfig.TlsCrt)
	cert, err := tls.LoadX509KeyPair(pConfig.TlsCrt, pConfig.TlsKey)
	if err != nil {
		log.Fatalf("dial: loadkeys: %s", err)
	}
	config := tls.Config{Certificates: []tls.Certificate{cert}, InsecureSkipVerify: true}
	conn, err := tls.Dial(pConfig.NetworkProtocol, pConfig.IP, &config)
	if err != nil {
		log.Fatalf("dial: %s", err)
	}

	log.Println("dial: connected to: ", conn.RemoteAddr())
	state := conn.ConnectionState()

	log.Println("dial: handshake: ", state.HandshakeComplete)

	return conn
}

func NewListener(pConfig Config) net.Listener {
	cert, err := tls.LoadX509KeyPair(pConfig.TlsCrt, pConfig.TlsKey)
	if err != nil {
		log.Fatalf("server: loadkeys: %s", err)
	}
	config := tls.Config{Certificates: []tls.Certificate{cert}, InsecureSkipVerify: true}
	config.Rand = rand.Reader
	service := pConfig.IP
	listener, err := tls.Listen(pConfig.NetworkProtocol, service, &config)
	if err != nil {
		log.Fatalf("server: listen: %s", err)
	}
	log.Printf("server: listening: %s", listener.Addr().String())

	return listener
}
