package universal

import "sync"

type ThreadSaveData struct {
	lock   sync.Mutex
	buffer []byte
}

func (tsd *ThreadSaveData) Write(data []byte) {
	tsd.lock.Lock()
	tsd.buffer = data
}

func (tsd *ThreadSaveData) Read() []byte {
	return tsd.buffer
}

func (tsd *ThreadSaveData) Lock() {
	tsd.lock.Lock()
}

func (tsd *ThreadSaveData) Unlock() {
	tsd.lock.Unlock()
}

func (tsd *ThreadSaveData) WriteUnsave(data []byte) {
	tsd.buffer = data
}
