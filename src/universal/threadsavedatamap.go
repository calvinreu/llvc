package universal

import (
	"encoding/json"
	"sync"
)

type ThreadSaveMapD struct {
	mutex      sync.Mutex
	map_       map[string][]byte
	buffersize int
}

func (tsm *ThreadSaveMapD) Init(buffersize_ int) {
	tsm.map_ = make(map[string][]byte)
	tsm.buffersize = buffersize_
}

func (tsm *ThreadSaveMapD) Len() int {
	return len(tsm.map_)
}

func (tsm *ThreadSaveMapD) Lock() {
	tsm.mutex.Lock()
}

func (tsm *ThreadSaveMapD) Unlock() {
	tsm.mutex.Unlock()
}
func (tsm *ThreadSaveMapD) At(index string) []byte {
	return tsm.map_[index]
}

//AddUnsave add index at value this operation is NOT thread save
func (tsm *ThreadSaveMapD) AddUnsave(index string) {
	tsm.map_[index] = make([]byte, tsm.buffersize)
}

//RemoveUnsave remove index this operation is NOT thread save
func (tsm *ThreadSaveMapD) RemoveUnsave(index string) {
	delete(tsm.map_, index)
}

//Add value at index thread save operation
func (tsm *ThreadSaveMapD) Add(index string) {
	tsm.mutex.Lock()
	tsm.map_[index] = make([]byte, tsm.buffersize)
	tsm.mutex.Unlock()
}

//Remove index from map thread save operation
func (tsm *ThreadSaveMapD) Remove(index string) {
	tsm.mutex.Lock()
	delete(tsm.map_, index)
	tsm.mutex.Unlock()
}

//Marshal json marshal of map
func (tsm *ThreadSaveMapD) Marshal() ([]byte, error) {
	return json.Marshal(tsm.map_)
}

//UnMarshal init operation not thread save should not be used after map was used
func (tsm *ThreadSaveMapD) UnMarshal(data []byte) error {
	return json.Unmarshal(data, &tsm.map_)
}
