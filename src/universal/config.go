package universal

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	IP, NetworkProtocol, TlsKey, TlsCrt    string
	MaxUsers, PullTimer, ConnectionTimeout int
}

func LoadConfig(filename string) (Config, error) {
	var config Config
	data, err := os.ReadFile(filename)
	if err != nil {
		return config, err
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		return config, err
	}
	return config, nil
}

func SaveConfig(filename string, config Config) error {
	data, err := json.Marshal(config)
	if err != nil {
		return err
	}
	fmt.Println(string(data))
	return os.WriteFile(filename, data, os.ModePerm)
}
