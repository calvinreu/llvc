package universal

import (
	"sort"
	"sync"
)

type SharedChannel struct {
	channelLock sync.Mutex
	channels    map[string]chan string
}

//Init SharedChannel NOT thread save should only be used once
func (sharedChannel *SharedChannel) Init() {
	sharedChannel.channels = make(map[string]chan string)
}

//ListIndex returns a list of every index
func (sharedChannel *SharedChannel) ListIndex() string {
	sharedChannel.channelLock.Lock()
	var List string

	keys := make([]string, 0, len(sharedChannel.channels))
	for k := range sharedChannel.channels {
		keys = append(keys, k)
	}
	sharedChannel.channelLock.Unlock()
	sort.Strings(keys)

	for _, i := range keys {
		List += i + "\n"
	}
	return List
}

func (sharedChannel *SharedChannel) Send(value string) {
	sharedChannel.channelLock.Lock()
	for _, i := range sharedChannel.channels {
		i <- value
	}
	sharedChannel.channelLock.Unlock()
}

func (sharedChannel *SharedChannel) GetChannel(ID string) chan string {
	sharedChannel.channelLock.Lock()
	retChan := sharedChannel.channels[ID]
	sharedChannel.channelLock.Unlock()
	return retChan
}

func (sharedChannel *SharedChannel) AddUser(ID string) {
	sharedChannel.channelLock.Lock()
	sharedChannel.channels[ID] = make(chan string, 100) //possible deadlock with multiple disconnects
	sharedChannel.channelLock.Unlock()
}

func (sharedChannel *SharedChannel) RemoveUser(ID string) {
	sharedChannel.channelLock.Lock()
	delete(sharedChannel.channels, ID)
	sharedChannel.channelLock.Unlock()
}

func (sharedChannel *SharedChannel) Lock() {
	sharedChannel.channelLock.Lock()
}

func (sharedChannel *SharedChannel) Unlock() {
	sharedChannel.channelLock.Unlock()
}
func (sharedChannel *SharedChannel) SendUnsave(value string) {
	for _, i := range sharedChannel.channels {
		i <- value
	}
}

func (sharedChannel *SharedChannel) AddUserUnsave(ID string) {
	sharedChannel.channels[ID] = make(chan string, 100) //possible deadlock with multiple disconnects
}

func (sharedChannel *SharedChannel) RemoveUserUnsave(ID string) {
	sharedChannel.channelLock.Lock()
	delete(sharedChannel.channels, ID)
	sharedChannel.channelLock.Unlock()
}
