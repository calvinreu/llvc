package universal

import (
	"encoding/json"
	"fmt"
	"os"
)

type Profile struct {
	Name, Description, IP string
}

func (profile *Profile) Unmarshal(data []byte) error {
	return json.Unmarshal(data, profile)
}

func (profile *Profile) Marshal() ([]byte, error) {
	return json.Marshal(profile)
}

func (profile *Profile) Load(filename string) error {
	data, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	return profile.Unmarshal(data)
}

func (profile *Profile) Save(filename string) error {
	data, err := profile.Marshal()
	if err != nil {
		return err
	}
	fmt.Println(string(data))
	return os.WriteFile(filename, data, os.ModePerm)
}
