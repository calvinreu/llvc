package universal

import (
	"encoding/json"
	"sync"
)

type ThreadSaveMap struct {
	mutex sync.Mutex
	map_  map[string]Profile
}

func (tsm *ThreadSaveMap) Init() {
	tsm.map_ = make(map[string]Profile)
}

func (tsm *ThreadSaveMap) Len() int {
	return len(tsm.map_)
}

func (tsm *ThreadSaveMap) Lock() {
	tsm.mutex.Lock()
}

func (tsm *ThreadSaveMap) Unlock() {
	tsm.mutex.Unlock()
}
func (tsm *ThreadSaveMap) At(index string) Profile {
	return tsm.map_[index]
}

//AddUnsave add index at value this operation is NOT thread save
func (tsm *ThreadSaveMap) AddUnsave(index string, value Profile) {
	tsm.map_[index] = value
}

//RemoveUnsave remove index this operation is NOT thread save
func (tsm *ThreadSaveMap) RemoveUnsave(index string) {
	delete(tsm.map_, index)
}

//Add value at index thread save operation
func (tsm *ThreadSaveMap) Add(index string, value Profile) {
	tsm.mutex.Lock()
	tsm.map_[index] = value
	tsm.mutex.Unlock()
}

//Remove index from map thread save operation
func (tsm *ThreadSaveMap) Remove(index string) {
	tsm.mutex.Lock()
	delete(tsm.map_, index)
	tsm.mutex.Unlock()
}

//Marshal json marshal of map
func (tsm *ThreadSaveMap) Marshal() ([]byte, error) {
	return json.Marshal(tsm.map_)
}

//UnMarshal init operation not thread save should not be used after map was used
func (tsm *ThreadSaveMap) UnMarshal(data []byte) error {
	return json.Unmarshal(data, &tsm.map_)
}
