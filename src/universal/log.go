package universal

import (
	"log"

	"github.com/natefinch/lumberjack"
)

func SetLogFile(filename string) {
	log.SetOutput(&lumberjack.Logger{
		Filename:   filename,
		MaxSize:    10, // megabytes
		MaxBackups: 5,
		MaxAge:     4, // days
	})
	log.Println("\n\n\n      Start\n------------")
}
