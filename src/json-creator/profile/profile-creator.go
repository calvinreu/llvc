package main

import (
	"flag"
	"fmt"
	"llvc/universal"
)

func main() {
	var profile universal.Profile
	filename := flag.String("destination", "", "destination to put the config in")
	flag.StringVar(&profile.Name, "Name", "", "")
	flag.StringVar(&profile.Description, "Description", "", "")
	flag.Parse()
	err := profile.Save(*filename)
	if err != nil {
		fmt.Println(err)
	}
}
