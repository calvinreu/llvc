package main

import (
	"flag"
	"fmt"
	"llvc/universal"
)

func main() {
	var config universal.Config
	filename := flag.String("destination", "", "destination to put the config in")
	flag.StringVar(&config.IP, "IP", "", "IP of Parser-Server")
	flag.StringVar(&config.NetworkProtocol, "NetworkProtocol", "tcp", "")
	flag.StringVar(&config.TlsKey, "TLSKey", "../ressources/default/client/tcp/default.key", "")
	flag.StringVar(&config.TlsCrt, "TLSCrt", "../ressources/default/client/tcp/default.crt", "")
	flag.IntVar(&config.MaxUsers, "MaxUsers", 20, "")
	flag.IntVar(&config.PullTimer, "PullTimer", 500, "")
	flag.IntVar(&config.ConnectionTimeout, "ConnTimeout", 10, "")
	flag.Parse()
	err := universal.SaveConfig(*filename, config)
	if err != nil {
		fmt.Println(err)
	}
}
