#! /bin/bash

#load config file
. $1

#create and enter keydir
mkdir "$LOCATION"
cd "$LOCATION"
echo created dir

#gen private key
openssl genrsa  -out "$KEYNAME".key 4096
echo private key created

#create Certificate Authority
openssl req -x509 -new -nodes -key "$KEYNAME".key -sha256 -days "$TIME" -out "$KEYNAME".crt -subj "/CN="$CERTIFICATE_NAME"/C="$COUNTRY"/ST="$STATE"/L="$CITY"/O="$ORG"" -passout pass:"$PASSWORD"
echo ca created