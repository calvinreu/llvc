#! /bin/bash

EXITTIMER="0.5"
PARSERTIMER1="1.5"
PARSERTIMER2="3"
SERVEREXIT="6"

if [ "$2" == "extensive" ]
then
    echo "extensive"
    EXITTIMER="5"
    PARSERTIMER1="5"
    PARSERTIMER2="15"
    SERVEREXIT="23"
fi

run_client () { 
    (sleep $1; echo "parser-test"; (sleep $2; echo "exit")) | go run ./client "-log_file=tmp/logs/$4" "-profile_file=tmp/profiles/$4" > "$3";
    #format output
    sed -i "/llvc/d" "$3"
    sed -i -e '/^$/d' "$3"    
}

match_iplists () {
    local FILES
    FILES=$(ls $1)
    FILES=(${FILES[@]})
    RefFile=${FILES[0]}
    for file in ${FILES[@]}
    do
        if [ "$(cat "$1/$file")" != "$(cat "$1/$RefFile")" ]
        then
            exit 1
        fi
    done
}

inv_match_iplists () {
    local FILES
    FILES=$(ls $1)
    FILES=(${FILES[@]})
    for file in ${FILES[@]}
    do
        if [ "$(cat "$1/$file")" == "$2" ]
        then
            exit 1
        fi
    done
}

check_len () {
    local FILES
    FILES=$(ls $1)
    FILES=(${FILES[@]})
    for file in ${FILES[@]}
    do 
        if [ "$(sed -n "$=" "$1/$file")" != "$2" ]
        then
            exit 1
        fi
    done
}

create_profile () {
    go run ./json-creator/profile/ "-destination=./tmp/profiles/$1" "-Name=$1" "-Description=$1"
}

cd src

mkdir tmp
mkdir tmp/1
mkdir tmp/2
mkdir tmp/logs
mkdir tmp/profiles
for i in {0..9}
do
    create_profile $i
done



(sleep "$SERVEREXIT"; echo "exit") | go run ./ip-server > /dev/null &
sleep 0.5
for i in {0..4}
do
    run_client $PARSERTIMER1 $EXITTIMER "tmp/1/$i" $i &
    sleep 0.001
done
for i in {5..9}
do
    run_client $PARSERTIMER2 $EXITTIMER "tmp/2/$i" $i &
    sleep 0.001
done

echo "clients started"

sleep $SERVEREXIT
# make sure clients have finshed
match_iplists "tmp/1"
check_len "tmp/1" "10"

echo "checked first clients"

match_iplists "tmp/2"
check_len "tmp/2" "5"

echo "checked second clients"

cd ../

if [ "$1" != "pipeline" ]
then
    rm logs/ip_parser.log; rm logs/client.log;
    rm -r src/tmp
fi