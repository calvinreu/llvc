# LLVC

Low Latency Voice Chat: a voice chat API which sends the audio packages directly to the other users instead of a server to reduce latency
Decentrelasing the transmittions to decrease the latency of voice chat hopefully enabling the abillity to sing usw... over voice chat.

## Roadmap
 - add test for ip parser
 - add log remover
 - add pipeline
 - use ip parser to pas profile info
 - add a server sending audio packages
 - add a client regularly receiving audio packages
 - add a connection system which can automaticly connect to the server of every participant in a voice chat
 - integrate input devices as audio source for the server
 - integrate some form of audio player on the client side
v1
 - try to train an AI to predict a couple of milliseconds of audio to give the impression of 0 latency.
v2

## License
this project is licenced under the GNU Affero General Public License v3.0

## Project status
this project is in early development with no usefull way to test


## execution
the default examples should all be executed from the base dir